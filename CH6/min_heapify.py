from math import floor

def father(i):
    return int(floor((i-0.1)/2))
def left(i):
    return 2*i +1
def right(i):
    return 2*(i+1) 
  
def min_heapify(Heap, parent):
    '''
    This function takes a min-heap with element (Heap[parent]) that does not 
    mantain the min-heap property and corrects it
    T(n) = O(log(n))
    '''
    l = left(parent)
    r = right(parent)
    
    if l >= len(Heap) or r >= len(Heap):
        return None    
    
    if Heap[l] < Heap[parent]:
        smallest = l
    else: smallest = parent
    if Heap[r] < Heap[smallest]:
        smallest = r
    if smallest != parent:
        Heap[parent], Heap[smallest] = Heap[smallest], Heap[parent]
        min_heapify(Heap,smallest)