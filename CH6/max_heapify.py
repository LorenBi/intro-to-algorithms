def left(i):
    return 2*i +1
def right(i):
    return 2*(i+1) 
    
def max_heapify(Heap, parent):
    '''
    This function takes a max-heap with element (Heap[parent]) that does not 
    mantain the max-heap property and corrects it
    T(n) = O(log(n))
    '''
    l = left(parent)
    r = right(parent)
    
    if l >= len(Heap) or r >= len(Heap):
        return None    
    
    if Heap[l] > Heap[parent]:
        largest = l
    else: largest = parent
    if Heap[r] > Heap[largest]:
        largest = r
    if largest != parent:
        Heap[parent], Heap[largest] = Heap[largest], Heap[parent]
        max_heapify(Heap,largest)