def left(i):
    return 2*i +1
def right(i):
    return 2*(i+1) 
    
def max_heapify_loop(Heap, parent):
    '''
    This function takes a max-heap with element (Heap[parent]) that does not 
    mantain the max-heap property and corrects it
    T(n) = O(log(n))
    '''
    while True:
        l = left(parent)
        r = right(parent)
        if l >= len(Heap) or r >= len(Heap):
            return None
    
        if Heap[l] < Heap[parent]:
            smallest = l
        else: smallest = parent
        if Heap[r] < Heap[smallest]:
            smallest = r
        if smallest != parent:
            Heap[parent], Heap[smallest] = Heap[smallest], Heap[parent]
            parent = smallest
        else:
            return None