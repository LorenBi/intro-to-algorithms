def recursiveInsertionSort(v):
    '''
    This function takes one list and sorts it with a recursive 
    insertion sort method
    '''
    #if the the list is of just of 2 elements I stop dividing
    if len(v)!=2:
        temp=v[0:len(v)-1]
        recursiveInsertionSort( temp)
        v[0:len(v)-1]=temp

    #I go through the list and find the place for v[len(v)]    
    i=len(v)-1
    while v[i-1]>v[i] and i>0:
        v[i-1], v[i]=v[i], v[i-1]
        i-=1
