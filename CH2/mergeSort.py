import random

def combine(A , B ):    
    #This function takes 2 sorted lists A and B
    #and returns a list with the elements of A and B sorted
    
    #I control if A or are empy
    if A==[] and B==[] :
        return []
    elif A==[] or B==[]:
        return A+B
    
    #I run through the 2 lists and create the sorted C 
    C=[]
    i=0
    j=0
    while(len(A)+len(B)>len(C)):
        if(A[i]<B[j]):
            C.append(A[i])
            i=i+1
            if(len(A)==i):
                C.extend(B[j:len(B)]) 
                #if i finish to go through one vector I put the last part of the other in C
        else:
            C.append(B[j])
            j=j+1
            if(len(B)==j):
                C.extend(A[i:len(A)])
    return C
    
def mergeSort(v):
    #This function takes any list and returns it sorted  with the mergesort methods
    #This function needs the function combine

    #I return the list if empty or with just one element
    if(len(v)==0 or len(v)==1):
        return v
        
    #I divide the list into 2 and cycle sort
    a = v[0:len(v)/2]
    b = v[len(v)/2:len(v)]
    a = mergeSort(a)
    b = mergeSort(b)
    
    #I combine the sorted lists into one sorted and return
    return combine(a,b)

random.seed