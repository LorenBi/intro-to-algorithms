def findIndexInSorted1(v,n):
    '''
    This function takes a list v and a number n
    It findes the index of the number n inside the list, 
    if there isn't, it will return a fractionary number
    The method implemented is a binary search using recursion
    Best case O(1)
    Average O(log2n)
    worst O(log2n)
    '''
    middle=len(v)//2
    if v[middle]!=n and len(v)==1:
        return 0.5
    elif v[middle]==n:
        return middle
    elif v[middle]>n:
        return findIndexInSorted1(v[0:middle],n)
    else:
        return findIndexInSorted1(v[middle+1:len(v)],n)+middle+1

def findIndexInSorted2(v,n):
        '''
    This function takes a list v and a number n
    It findes the index of the number n inside the list, 
    if there isn't, it will return None
    The method implemented is a binary search using iteration
    Best case O(1)
    Average O(log2n)
    worst O(log2n)
    '''    
    
    higher=len(v)
    lower=0
    while True:
        index= (higher-lower)//2+lower
        if v[index]==n:
            return index
        elif v[index]!=n and higher==lower:
            return None
        elif v[index]>n:
            higher=index
        elif v[index]<n:
            lower =index+1