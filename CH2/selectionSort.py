def selectionSort(v):
    '''
    This function tacks a list and return the list sorted
    The sorting method is the selection sort with best and worst case performace O(n^2)
    '''
    for i in range(len(v)):
        index=i
        for j in range(i+1,len(v)):
            if v[j]<=v[index]:
                index=j
        tmp=v[index]
        v[index]=v[i]
        v[i]=tmp
    return v
