def fast_find_sublist(v):
    '''
    This function takes a list and returns the indeces and sum of the 
    continuous sublist, whose sum is the maximum.
    Best, worst, average case are in linear time.
    '''
    sum_so_far = 0
    for i in range(0, len(v)):
        sum_so_far += v[i]
        if i==0 or (v[i] > sum_so_far and v[i]>max_sum) :
            left_index = i
            right_index = i+1
            max_sum = v[i]
            sum_so_far = v[i]  #we're not interested in the previous elements
        elif sum_so_far > max_sum and sum_so_far >= v[i]:
            right_index = i+1
            max_sum = sum_so_far
    return max_sum, left_index, right_index 