def multiply_square_matrix(A, B):
    '''
    multiply_square_matrix takes 2 square tuples of the same size and returns 
    the prodoct of the two.
    Best, worst and average case run in cubic time.
    '''
    if A[0] != list or B[0] != list:
        print "Please do not enter list, but square tuples"
        return None
    elif (len(A) == len(B) == len(A[0]) == len(B[0])):
        n = len(A)
        C = [[0 for x in range(n)] for x in range(n)]
        for i in range(n):
            for j in range(n):
                for k in range(n):
                    C[i][j] = C[i][j] + A[i][k]*B[k][j]
        return C
    else:
        print "Please use square-tuples of the same size"
        return None