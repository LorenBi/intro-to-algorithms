from numpy import array

def recursive_multiply_matrix(A,B):
    '''
    multiply_square_matrix takes 2 square matrix of the same size
    (divisible by 2!!!) and returns the prodoct of the two.
    Best, worst and average case run in cubic time.
    '''
    if len(A)==1:
        return A*B
        
    end = len(A)
    middle = end/2
    C = array([[0 for column in range(end)] for rows in range(end)])
    
    C[0:middle][:,0:middle] = recursive_multiply_matrix(A[0:middle][:,0:middle]
    , B[0:middle][:,0:middle]) + recursive_multiply_matrix(A[0:middle][:,middle:end], B[middle:end][:,0:middle])
    C[0:middle][:,middle:end]  = recursive_multiply_matrix(A[0:middle][:,0:middle], B[0:middle][:,middle:end]) + recursive_multiply_matrix(A[0:middle][:,middle:end], B[middle:end][:,middle:end])
    C[middle:end][:,0:middle] = recursive_multiply_matrix(A[middle:end][:,0:middle], B[0:middle][:,0:middle]) + recursive_multiply_matrix(A[middle:end][:,middle:end], B[middle:end][:,0:middle])
    C[middle:end][:,middle:end] = recursive_multiply_matrix(A[middle:end][:,0:middle], B[0:middle][:,middle:end]) + recursive_multiply_matrix(A[middle:end][:,middle:end], B[middle:end][:,middle:end])
    
    return C