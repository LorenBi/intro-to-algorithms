from numpy import inf

def slow_find_sublist(v):
    '''
    This function takes a list and returns the indeces and sum of the 
    continuous sublist, whose sum is the maximum.
    It uses the brute-force method, by checking every possible sublist.
    Its best, worst, average case are O(n**2).
    '''
    max_sum = -inf
    for i in range(0,len(v)):
        check_sum = 0
        for j in range(i,len(v)):
            check_sum += v[j]
            if check_sum > max_sum:
                max_sum = check_sum
                left_index = i
                right_index = j
    return max_sum, left_index, right_index+1  #+1 for the py syntax with lists