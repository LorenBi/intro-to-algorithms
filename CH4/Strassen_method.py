from numpy import empty, ceil, log2, hstack, vstack, zeros

def Strassen_method(A, B):
    '''
    This function takes two squared matrix (array of the library numpy) and 
    returns the product of the two.
    It uses the Strassen algorithm for the computation.
    
    This is not the most efficient way of implementing Strassen algorithm. But, 
    considering that the purpose of this function is only for learning, I
    prefered something readable to something efficient.
    '''    
    
    if len(A)==1:
        return A*B
    
    original_n = len(A)    
    n=2**ceil(log2(original_n))
    middle = n/2
    
    if n!=original_n:     #I add zeros if A,B dimension aren't in the form 2**n
        A = hstack((A, zeros((original_n, n-original_n))))
        B = hstack((B, zeros((original_n, n-original_n))))
        A = vstack((A, zeros((n-original_n, n))))
        B = vstack((B, zeros((n-original_n, n))))
        
    C = empty([n, n])
    A_11 = A[0:middle][:,0:middle]
    A_12 = A[0:middle][:,middle:n]
    A_21 = A[middle:n][:,0:middle]
    A_22 = A[middle:n][:,middle:n]
    B_11 = B[0:middle][:,0:middle]
    B_12 = B[0:middle][:,middle:n]
    B_21 = B[middle:n][:,0:middle]
    B_22 = B[middle:n][:,middle:n]
    
####### Strassen Algorithm ###########    
    
    S_1 = B_12 - B_22
    S_2 = A_11 + A_12
    S_3 = A_21 + A_22
    S_4 = B_21 - B_11
    S_5 = A_11 + A_22
    S_6 = B_11 + B_22
    S_7 = A_12 - A_22
    S_8 = B_21 + B_22
    S_9 = A_11 - A_21
    S_10 = B_11 + B_12
    
    P_1 = Strassen_method(A_11, S_1)
    P_2 = Strassen_method(S_2, B_22)
    P_3 = Strassen_method(S_3, B_11)
    P_4 = Strassen_method(A_22, S_4)
    P_5 = Strassen_method(S_5, S_6)
    P_6 = Strassen_method(S_7, S_8)
    P_7 = Strassen_method(S_9, S_10)
    
    C[0:middle][:,0:middle] = P_5 + P_4 - P_2  + P_6
    C[0:middle][:,middle:n] = P_1 + P_2
    C[middle:n][:,0:middle] = P_3 + P_4
    C[middle:n][:,middle:n] = P_5 + P_1 - P_3 - P_7
    
    return C[0:original_n][:,0:original_n] #I cut off the zeros I added