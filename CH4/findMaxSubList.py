from numpy import inf

def findMaxCrossingSubList(v,low, high, middle):
    '''
    findMaxCrossingSubList takes a list and 3 integers
    It returns a continuus sublist,whose elements have the biggest sum, but has the v[middle] element in it
    '''
    leftSum=-inf
    totSum=0
    for i in range(middle,low-1,-1):
        totSum=totSum+v[i]
        if totSum>leftSum:
            leftSum=totSum
            leftMax=i
    rightSum=-inf
    totSum=0
    for i in range(middle,high):
        totSum=totSum+v[i]
        if totSum>rightSum:
            rightSum=totSum
            rightMax=i
    return leftMax,rightMax,leftSum+rightSum-v[middle]
    
def findMaxSubList(v,low=0,high=-1):
    '''
    findMaxCrossingSubList takes a list and 2 integers
    It returns the indices of a continuus sublist,whose elements have the biggest sum and return its sum
    '''
    if high==-1:
        high=len(v)
    if high==low+1:
        return low,high,v[low]
    else:
        middle=(low+high)/2
        lowLeft,highLeft, sumLeft = findMaxSubList(v,low,middle)
        lowRight, highRight, sumRight = findMaxSubList(v,middle,high)
        lowMid, highMid, sumMid = findMaxCrossingSubList(v,low,high,middle)
        if sumLeft>=sumRight and sumLeft>=sumMid:
            return lowLeft,highLeft, sumLeft
        if sumRight>=sumLeft and sumRight>=sumMid:
            return lowRight, highRight, sumRight
        else:
            return lowMid, highMid, sumMid