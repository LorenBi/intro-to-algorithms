import math
import numpy
numpy.float128

filename = "distance.txt"
mynumbers = []
d=[0,0,0,0]
xv=[]
yv=[]
zv=[]
#I run through every line and that take the 4 numbers separeted by " " and put them in d
with open(filename) as f:
    for line in f:
        mynumbers.append([float(n) for n in line.strip().split(' ')])
for pair in mynumbers:
    try:
        d[0],d[1],d[2],d[3] = pair[0],pair[1],pair[2],pair[3]
        p=2000;q=2000; r=2000; s=3000; t=1500; u=1700
        
        #I calculate x y z and then check if z is positive or negative       
        x=(d[0]**2-d[1]**2+p**2)/(2*p)
        y=(d[0]**2-d[2]**2+q**2+r**2)/(2*r)-(q/r)*x
        z=math.sqrt(d[0]**2-x**2-y**2)                
        if abs((x-s)**2+(y-t)**2+(-z-u)**2-d[3]**2)<abs((x-s)**2+(y-t)**2+(z-u)**2-d[3]**2):
            z=-z
        xv.append(x)
        yv.append(y)
        zv.append(z)        
    except IndexError:
        print "A line in the file doesn't have enough entries."

#I calc the mean of the ith coordinate of every point and then round them up. 

a=math.ceil(numpy.mean(xv))
b=math.ceil(numpy.mean(yv))
c=math.ceil(numpy.mean(zv))

print a, b, c